import {
  app,
  BrowserWindow,
  Menu,
  Tray
} from 'electron';
const path = require('path');
import './api.js';

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require('electron-squirrel-startup')) { // eslint-disable-line global-require
  app.quit();
}

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;
let tray = null;
const createWindow = () => {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 500,
    height: 400,
  });
  // and load the index.html of the app.
  mainWindow.loadURL(`file://${__dirname}/index.html`);

  // Open the DevTools.
  mainWindow.webContents.openDevTools();

  // 当我们点击关闭时触发close事件，我们按照之前的思路在关闭时，隐藏窗口，隐藏任务栏窗口
  // event.preventDefault(); 禁止关闭行为(非常必要，因为我们并不是想要关闭窗口，所以需要禁止默认行为)
  mainWindow.on('close', (event) => {
    mainWindow.hide();
    mainWindow.setSkipTaskbar(true);
    event.preventDefault();
  });

  mainWindow.on('show', () => {
    tray.setHighlightMode('always');
  });
  mainWindow.on('hide', () => {
    tray.setHighlightMode('never');
  });
  //创建系统通知区菜单  mac的打包，要把.ico换成icns
  tray = new Tray(path.join(__dirname, './images/icon/app_logo.ico'));
  const contextMenu = Menu.buildFromTemplate([{
      label: '退出',
      click: () => {
        mainWindow.destroy();
      }
    }, //我们需要在这里有一个真正的退出（这里直接强制退出）
  ]);
  tray.setToolTip('Get-Mac');
  tray.setContextMenu(contextMenu);
  tray.on('click', () => { //我们这里模拟桌面程序点击通知区图标实现打开关闭应用的功能
    mainWindow.isVisible() ? mainWindow.hide() : mainWindow.show()
    mainWindow.isVisible() ? mainWindow.setSkipTaskbar(false) : mainWindow.setSkipTaskbar(true);
  });

  // Emitted when the window is closed.
  mainWindow.on('closed', () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
  });
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

//设置开机自启
app.setLoginItemSettings({
  openAtLogin: true,
  //苹果系统隐藏式启动
  openAsHidden: false
});
//Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow();
  }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.
// const contextMenu = Menu.buildFromTemplate([
//   {
//     type: 'checkbox',
//     label: '开机自启',
//     checked: app.getLoginItemSettings().openAtLogin,
//     click: function () {
//       if (!app.isPackaged) {
//         app.setLoginItemSettings({
//           openAtLogin: !app.getLoginItemSettings().openAtLogin
//           // path: process.execPath
//         })
//       } else {
//         app.setLoginItemSettings({
//           openAtLogin: !app.getLoginItemSettings().openAtLogin
//         })
//       }
//       var isOpen = app.getLoginItemSettings().openAtLogin;
//       var options = {
//         type: 'info',
//         buttons: ['确定'],
//         title: '开机启动',
//         message: ''
//       };
//       //console.log(dialog);
//       if (isOpen) {
//         options.message = "已启动！";
//         dialog.showMessageBox(options);
//       } else {
//         options.message = "已关闭！";
//         dialog.showMessageBox(options);
//       }
//       //console.log(!app.isPackaged);
//     }
//   }
// ])
// Menu.setApplicationMenu(contextMenu);

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.