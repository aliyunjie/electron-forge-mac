const express = require('express');
import { macCode, macCodeMD5 } from './mac.js';
const app = express();
app.listen(9008, () => {
    console.log('server start up!')
})

app.get('/getMac', (req, res) => {
    //设置允许跨域的域名，*代表允许任意域名跨域
    res.header("Access-Control-Allow-Origin", "*");
    //允许的header类型
    res.header("Access-Control-Allow-Headers", "content-type");
    //跨域允许的请求方式 
    res.header("Access-Control-Allow-Methods", "DELETE,PUT,POST,GET,OPTIONS");
    var result = {
        mac: "",
        sign: "",
        isSuccess: false
    }
    if (macCode && macCodeMD5) {
        result.mac = macCode;
        result.sign = macCodeMD5;
        result.isSuccess = true;
    }
    res.send(result);
})
