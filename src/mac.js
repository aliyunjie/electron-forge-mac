var md5 = require('md5');
var address = require('address');
var macCode = null;
address.mac(function (err, addr) {
    if(addr)
    macCode = addr.toUpperCase();
});
var macCodeMD5 = md5(macCode + "CustomizeYourOwnKey").toUpperCase();
export { macCode, macCodeMD5 }