### Electron-forge 获取本机MAC地址的客户端工具
&ensp;&ensp;基于electron-forge demo的二次开发  
&ensp;&ensp;这里只是个简单的应用，可以当做模板，去开发其他桌面应用  

#### 配置文件package.json

electronPackagerConfig:{  
&ensp;&ensp;"packageManager": "npm",  
&ensp;&ensp;"icon": "src/images/icon/app_logo",//应用图标  
&ensp;&ensp;"asar": true,//加密  
&ensp;&ensp;"overwrite": true//覆盖打包  
}  
注意：      
&ensp;&ensp;这里的配置要特别注意，配置的所有系统的，win/mac/linux...  
&ensp;&ensp;icon的路径最后的文件不需要加后缀，electron-forge会检测到系统，自己添加对应的文件

dependencies：  
&ensp;&ensp;这个是正式打包需要的配置，必要的包都要配置在这里


#### 主进口文件index.js  
主要功能：
1. 创建窗体，加载
2. 关闭托管后台
3. 开机自启
4. 开机自启的开关（已注释掉了，开发任务不需要）  

#### 对外接口api.js  
&ensp;&ensp;提供浏览器访问，获取相关信息

#### 获取mac地址mac.js  
&ensp;&ensp;获取本机的mac地址，加密

